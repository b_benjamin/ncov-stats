import Vue from 'vue'
import App from './App.vue'
import NumeralFilter from './filters/numeral'

Vue.config.productionTip = false

Vue.filter('numeral', NumeralFilter)

new Vue({
  render: h => h(App),
}).$mount('#app')
